function [U_law, J] = EV_StoDynProg(EE, SOE_fin, SOE, XI, TT, Prob_xi)
% computes the optimal strategy for a vehicle charging using stochastic
% dynamic programming
% 
% inputs : EE : vector of the discretized value of the battery capacity
%          SOE_fin : desired final state of energy
%          SOE : set of possible states of energy
%          XI : vector of the discretized value of the coordination
%             coefficient
%          TT : discretized time vector
%          Prob_xi : matrix of probability density of xi
% output : U_law : matrix of the optimal policy
%          cost_opt : optimal cost when following U_law

DeltaT = TT(1)-TT(2);
% results preallocation
U_law = zeros(length(EE),length(SOE_fin),length(SOE),length(XI),length(TT)-1);
J = zeros(length(EE),length(SOE_fin),length(SOE),length(XI),length(TT));

% evaluation of the cost associated to the final situation
final_cost = EV_final_cost(SOE_fin, SOE);
J_final = zeros(length(EE),length(SOE_fin),length(SOE),length(XI));
for i_e = 1:length(EE)
    for i_xi = 1:length(XI)
        J_final(i_e,:,:,i_xi) = EE(i_e)*final_cost;
    end
end

% parallelized state loop on battery capacity
parfor i_e = 1:length(EE)
    E = EE(i_e);
%     disp(E)
    [U_par, J_par] = EV_DynProg_par(E, SOE_fin, SOE, XI, TT, Prob_xi, ...
        DeltaT, squeeze(J_final(i_e,:,:,:)));
    J(i_e,:,:,:,:) = J_par;
    U_law(i_e,:,:,:,:) = U_par;
end
end

function [U_par, J_par] = EV_DynProg_par(E, SOE_fin, SOE, XI, TT, ...
    Prob_xi, DeltaT, J_final)
U_par = zeros(length(SOE_fin), length(SOE),length(XI),length(TT)-1);
J_par = zeros(length(SOE_fin), length(SOE),length(XI),length(TT));
J_par(:,:,:,end) = J_final;
for i_soe_fin = length(SOE_fin):-1:1
    % Backward Resolution Loop
    for i_t = length(TT)-1:-1:1
        J_future = squeeze(J_par(i_soe_fin,:,:,i_t+1));
        % state loop : coordination coefficient
        for i_xi = 1:length(XI)
            xi = XI(i_xi);
            % state loop : state of energy
            for i_soe = 1:length(SOE)
                soe = SOE(i_soe);
                prob_xi = squeeze(Prob_xi(i_xi,i_soe_fin,i_soe,i_t,:));
                [P_opt,min_cost,ret] = EV_DP_Minimisation(...
                    E, soe, SOE, xi, prob_xi, DeltaT, J_future);
                if ret==-1
                    disp([i_e, i_soe_fin, i_t,i_xi,i_soe])
                end
                U_par(i_soe_fin,i_soe,i_xi,i_t) = P_opt;
                J_par(i_soe_fin,i_soe,i_xi,i_t) = min_cost;
            end
        end
    end
end
end
