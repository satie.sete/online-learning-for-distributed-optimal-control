%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time step specification %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
dT_init = 1/12; % in hours
N_decim = 12; % decimation factor
dT = N_decim*dT_init;

%% wind generation 
% from BPA wind open data set
% https://transmission.bpa.gov/Business/Operations/Wind/
% installed wind generation capacity is stable between 2014, december 19th
% and 2017, december 14th. 
% Imported data are in MW, then converted to W. 

P_w_2015_1 = import_prod_2015_1('WindGenTotalLoadYTD_2015.xls');
P_w_2015_2 = import_prod_2015_2('WindGenTotalLoadYTD_2015.xls');
P_w_2016_1 = import_prod_2016_1('WindGenTotalLoadYTD_2016.xls');
P_w_2016_2 = import_prod_2016_2('WindGenTotalLoadYTD_2016.xls');
P_w_2017_1 = import_prod_2017_1('WindGenTotalLoadYTD_2017.xls');
P_w_2017_2 = import_prod_2017_2('WindGenTotalLoadYTD_2017.xls');

P_w = [P_w_2015_1 ; P_w_2015_2 ; ...
    P_w_2016_1 ; P_w_2016_2 ; ...
    P_w_2017_1 ; P_w_2017_2]*1e6 ; 
P_w(isnan(P_w)) = 0;
P_w = P_w(1:N_decim:end);

save('P_w.mat','P_w','dT')

%% EV behaviour
% from Commissariat General au eéveloppement Durable, Electric vehicles in 
% perspective, cost benefit analysis and potential demand (original title 
% in french: Les vehicules electriques en perspective, Analyse couts-
% avantages et demande potentielle), 2011

N_ev = 200; % numbers of vehicles
N_days = 365+366+365;
[H_list, E_list, soe_0_list, soe_fin_list, fleet_prms] = ...
    EV_generator(N_ev,N_days);
if N_decim==12
    H_list = floor(H_list);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% H_list is affected for N_decim>6. Only N_decim=12 (dT=1h) is handled %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% vehicle data pre-processing
T_list = 0:dT:24-dT;
T_charg = zeros(N_days,N_ev,length(T_list)); % remaining charging time
EV_flag = zeros(N_days+1,N_ev,length(T_list)); % vehicle presence flag
n_ev = zeros(N_days,length(T_list)); % number of present vehicles
soe_fin = zeros(N_days,N_ev,length(T_list)); % desired final state of energy
soe = zeros(N_days,N_ev,length(T_list)); % current state of energy
for d = 1:N_days
    for n = 1:N_ev
       EV_flag(d,n,:) = (T_list<H_list(d,n,1)) + ...
           (H_list(d,n,2)<=T_list).*(T_list<H_list(d,n,3)) + ...
           (H_list(d,n,4)<=T_list);
    end
    n_ev(d,:) = sum(squeeze(EV_flag(d,:,:)),1);
    for n=1:N_ev
        ind = (T_list<=H_list(d,n,1));
        T_charg(d,n,ind) = H_list(d,n,1)-T_list(ind);
        soe_fin(d,n,ind) = soe_fin_list(d,n,1);       
        ind = (H_list(d,n,2)<=T_list)&(T_list<=H_list(d,n,3));
        T_charg(d,n,ind) = H_list(d,n,3)-T_list(ind);
        soe_fin(d,n,ind) = soe_fin_list(d,n,2);
        ind = find(ind==1,1,'first');
        soe(d,n,ind) = soe_0_list(d,n,1);
        ind = (H_list(d,n,4)<=T_list);
        if d<N_days
            T_charg(d,n,ind) = 24 + H_list(d+1,n,1)-T_list(ind);
            soe_fin(d,n,ind) = soe_fin_list(d+1,n,1);
            ind = find(ind==1,1,'first');
            soe(d,n,ind) = soe_0_list(d,n,2);
        else
            % arbitrary departure at noon at the end of the test case
            T_charg(d,n,ind) = 24 + 12 - T_list(ind);
            soe_fin(d,n,ind) = 1;
            ind = find(ind==1,1,'first');
            soe(d,n,ind) = soe_0_list(d,n,2);
        end
    end
end
EV_flag(end,:,:) = [ones(N_ev,length(T_list)/2) zeros(N_ev,length(T_list)/2)];

save('EV.mat','H_list','E_list','soe_0_list','soe_fin_list','fleet_prms', ...
    'T_charg','EV_flag','n_ev','soe_fin','soe','T_list');