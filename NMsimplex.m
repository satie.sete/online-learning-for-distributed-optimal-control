function [P_opt,V_min,ret] = NMsimplex(P0, P_min, P_max, soe, SOE, E, ...
    xi, prob_xi, DeltaT, J_future)
% 1D version of a Nelder Mead simplex solver for the optimal stored power. 
% Returns the optimal power and the minimal value of the total cost. 
% inputs : P0 : initial guess (2 values)
%          P_min : minimum acceptable power
%          P_max : maximum acceptable power
%          soe : current state of energy
%          SOE : set of possible states of energy
%          E : vehicle battery capacity
%          xi : value of the coordination coefficient
%          prob_xi : vector of the probability of the future value of
%                   xi
%          DeltaT : time step in hours
%          J_future : cost of the future state (2D matrix)
% output : P_opt : optimal power
%          V_min : minimal cost at the optimal power
%          ret : =-1 if not converged after 100 iterations
%                =number of iteration at convergence otherwise
    
% simplex parameters default values
alpha = 1;
gamma = 2;
rho = -0.5;
sigma = 0.5;
rel_tol = 1e-5;
abs_tol = 1e-2;

% function to be minimized
obj_fun = @(P)(EV_calc_cost(P,soe,SOE,E,xi,prob_xi,DeltaT,J_future));

% boundaries
x_min = P_min;
x_max = P_max;
% INITIAL SIMPLEX
% it is a 1D simplex version. Then the inital simplex is the initial
% guess. 
X = P0;
F = zeros(size(X));
n=1;
cond = 1;
while cond
    X0=X;
    % EVALUATION
    for i = 1:length(X)
        F(i) = obj_fun(X(i));
    end
    % ORDERING
    [F,ind] = sort(F);
    X = X(ind);
    % CENTROID
    % as it is 1D, centroid is the first point
    x0 = X(1);
    % REFLECTION
    xr = x0 + alpha*(x0-X(2));
    xr = max(min(xr,x_max),x_min);
    if obj_fun(xr) < F(1)
        % EXPANSION
        xe = x0 + gamma*(x0-X(2));
        xe = max(min(xe,x_max),x_min);
        if obj_fun(xe)<obj_fun(xr)
            X = [X(1); xe];
        else
            X = [X(1); xr];
        end
    else
        % CONTRACTION
        xc = x0 + rho*(x0-X(2));
        xc = max(min(xc,x_max),x_min);
        if obj_fun(xc)<F(2)
            X = [X(1) ; xc];
        else
            % REDUCTION
            X(2) = X(1) + sigma*(X(2)-X(1));
        end
    end
    n=n+1;
    tol = abs_tol + min(abs(X))*rel_tol;
    cond = (norm(X-X0)>tol)&(n<100);
end
ret = n;
if (n==100)&&(norm(X-X0)>tol)
    ret = -1;
end
P_opt = X(1);
V_min = F(1);
end