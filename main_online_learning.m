%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ONLINE LEARNING FOR DISTRIBUTED OPTIMAL CONTROL OF AN ELECTRIC VEHICLE %
% FLEET                                                                  % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% implementation and test case of the method proposed in "Le Goff Latimier,
% R, Cherot, G and Ben Ahmed, H, Online learning for distributed optimal
% control of an electric vehicle fleet, PSCC 2022, submitted"

% Execution time on Intel i7-8700: 24hours
% RAM requirement : 16Go
% results volume : 2.5Go

clearvars
close all
clc

%% load time series
% if these .mat files do not exist yet, run load_test_case.m to generate
% them. 

% wind
load('P_w.mat')
P_w = P_w/10000; 
% wind power is scalled to produce as much energy the fleet needs for
% charging over a year.

% vehicles
load('EV.mat')

[N_days,N_ev,~] = size(H_list);

%% charging strategy state space discretisation

% battery capacity
E_min = 10e3; % minimum value
E_max = 85e3; % maximum value
N_E = 16; % number of points
EE = linspace(E_min,E_max,N_E)';
% state of energy
N_SOE = 11; % number of points
SOE = linspace(0,1,N_SOE)';
% desired final state of energy
N_SOE = 11; % number of points
SOE_fin = linspace(0,1,N_SOE)';
% coordination coefficient
xi_min = -1e6; % minimum value
xi_max =  1e6; % maximum value
N_xi = 21; % number of points, should be odd value
XI = linspace(xi_min,xi_max,N_xi)';
% charging time
TT = fliplr(T_list);

%% initial probability density of xi evolution
Prob_xi = zeros(length(XI),length(SOE_fin),length(SOE),length(TT),length(XI));
Prob_xi(:,:,:,:,floor(N_xi/2)+1) = ...
    ones(length(XI),length(SOE_fin),length(SOE),length(TT),1);
% at initialisation, all vehicles uses the same strategy
EV_strat = ones(N_ev,1);

%% initial charging strategy
U_law(1,:,:,:,:,:) = EV_calc_strat(EE,SOE_fin,SOE,TT,XI,Prob_xi);
iter = 1;
u_law = squeeze(U_law(1,:,:,:,:,:));
str = strcat('strat_iter_',num2str(iter),'.mat');
save(str,'Prob_xi','u_law');

%% online learning parameters
set_online_prms;

%% online learning loop
% results preallocation
cpt_day = 0;
P = zeros(N_days,N_ev,length(T_list));
P_tot = zeros(N_days,length(T_list));
u = zeros(N_days,length(T_list));
costs = zeros(N_days,3);
u_init = u(1,1);
P_init = squeeze(P(1,:,1));
% daily loop
for d = 1:N_days
    disp(d);
    tic;
    % simulation over a day
    [P(d,:,:), soe(d,:,:), u(d,:), soe_tomorrow, P_tot(d,:), costs(d,:)] = ...
        daily_simul(EE, SOE_fin, SOE, XI, TT, U_law, EV_strat, ...
        squeeze(T_charg(d,:,:)), squeeze(EV_flag(d,:,:)), ...
        squeeze(soe_fin(d,:,:)), squeeze(soe(d,:,:)), E_list, ...
        squeeze(soe_fin_list(d,:,:)), ...
        P_w(length(TT)*(d-1)+1:length(TT)*d), u_init, P_init);
    soe(d+1,:,1) = soe_tomorrow;
    cpt_day = cpt_day + 1;
    % update
    if cpt_day == online_prms.N_days
        cpt_day = 0;
        if online_prms.k<online_prms.share
            online_prms.k = online_prms.k+1;
        else
            online_prms.k = 1;
        end
        iter = iter + 1;
        % xi modeling update
        [Prob_xi,EV_strat] = prob_xi_online(XI,P(1:d,:,:),u(1:d,:),...
            SOE,soe(1:d,:,:),Prob_xi, EV_strat,online_prms,'xi');
        % strategy update
        U_law(online_prms.k,:,:,:,:,:) = ...
            EV_calc_strat(EE,SOE_fin,SOE,TT,XI,Prob_xi);
        u_law = squeeze(U_law(online_prms.k,:,:,:,:,:));
        str = strcat('strat_iter_',num2str(iter),'.mat');
        save(str,'Prob_xi','u_law');
    end
    toc
end

save('results','-v7.3')
