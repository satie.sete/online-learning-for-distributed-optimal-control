function [P_min,P_max] = EV_constraint_P(soe, E, DeltaT)
% constraints on the vehicle charging power
% input : soe : current state of energy
%         E : vehicle battery capacity
%         DeltaT : time step in hours
% output : P_min : minimum acceptable power
%          P_max : maximum acceptable power

P_rated = 150e3; % rated power of the charging station
P_min = max(-P_rated,-soe*E/DeltaT);
P_max = min(P_rated,(1-soe)*E/DeltaT);
end