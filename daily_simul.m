function [P, soe, u, soe_tomorrow, P_tot, costs] = daily_simul(...
    EE, SOE_fin, SOE, XI, TT, U_law, EV_strat,...
    T_charg, EV_flag, soe_fin, soe, E_list, soe_fin_list, ...
    P_w, u_init, P_init)
% performs the simulation of the fleet behaviour and optimal charging
% routine over a day. 

% inputs : 
%   EE : vector of the discretized value of the battery capacity
%   SOE_fin : vector of the discretized value of desired final state of energy
%   SOE : vector of the discretized value of states of energy
%   XI : vector of the discretized value of the coordination coefficient
%   TT : vector of the discretized value of time vector
%   U_law : matrix of the optimal policy
%   EV_strat : strategy to be used by each vehicle
%   T_charg : remaining charging time
%   EV_flag : matrix of vehicle presence flag
%   soe_fin : matrix (N_days,N_ev,2) of the desired final state of energy
%   soe : current state of energy
%   E_list : matrix (N_days,N_ev) of the battery capacities
%   soe_fin_list : matrix (N_days,N_ev,length(T_list)) of the desired final 
%                  states of energy
%   P_w : wind power production
%   u_init : initial dual variable (warm start)
%   P_init : initial charging power (warm start)
% outputs : 
%   P : matrix (N_days,N_ev,length(T_list)) of charging power
%   soe : matrix (N_days,N_ev,length(T_list)) of states of energy 
%   u : list (length(T_list)) dual variable
%   soe_tomorrow : list (N_ev) of soe for the next day
%   P_tot : list (length(T_list)) total power of the fleet
%   costs : list (3) of daily costs

N_ev = length(E_list); % number of vehicles
T_list = flip(TT); % time list
dT = T_list(2) - T_list(1); % time step

%% temporal simulation
% results preallocation
P = zeros(N_ev,length(T_list)); % charging power
P_loss = zeros(N_ev,length(T_list)); % losses power
u = zeros(length(T_list),1); % dual variable
soe_tomorrow = zeros(N_ev,1); % soe for next day
soe_fin_real = zeros(N_ev,3); % final soe for each trip
% time loop
for i_t = 1:length(T_list)
    % warm start initialisation
    ind = (EV_flag(:,i_t)==1);
    [P_prev, u_prev] = warm_start(u,P,i_t,ind,u_init,P_init);
    % sharing problem
    [P(ind,i_t),u(i_t)] = sharing(dT,P_w(i_t),...
        EE, SOE_fin, SOE, XI, TT, U_law, EV_strat(ind),...
        E_list(ind), soe_fin(ind,i_t), ...
        soe(ind,i_t), T_charg(ind,i_t),P_prev,u_prev);
    % dynamic of the battery charging
    if i_t<length(T_list) % within the day
        for n = 1:N_ev
            if ind(n)&&(dT<T_charg(n,i_t))
                % vehicle will be still there at next time step
                [soe(n,i_t+1),P_loss(n,i_t),~] = ...
                    EV_dynamic(E_list(n), soe(n,i_t), P(n,i_t), dT);
            elseif ind(n)&&(T_charg(n,i_t)<=dT)
                % vehicle will move before next time step
                if soe_fin_real(n,3)==0
                    % first trip of the day
                    [soe_fin_real(n,1),P_loss(n,i_t),~] = ...
                        EV_dynamic(E_list(n), soe(n,i_t), P(n,i_t), dT);
                    soe_fin_real(n,3) = 1;
                else
                    % second trip of the day
                    [soe_fin_real(n,2),P_loss(n,i_t),~] = ...
                        EV_dynamic(E_list(n), soe(n,i_t), P(n,i_t), dT);
                end
            end
        end
    else % last time step of the day
        for n = 1:N_ev
            if ind(n)&&(dT<T_charg(n,i_t))
                % vehicle will be still there at the first time step of the
                % next day
                [soe_tomorrow(n),P_loss(n,i_t),~] = ...
                    EV_dynamic(E_list(n), soe(n,i_t), P(n,i_t), dT);
            elseif ind(n)&&(T_charg(n,i_t)<=dT)
                % vehicle will move before next time step
                if soe_fin_real(n,3)==0
                    % first trip of the day
                    [soe_fin_real(n,1),P_loss(n,i_t),~] = ...
                        EV_dynamic(E_list(n), soe(n,i_t), P(n,i_t), dT);
                    soe_fin_real(n,3) = 1;
                else
                    % second trip of the day
                    [soe_fin_real(n,2),P_loss(n,i_t),~] = ...
                        EV_dynamic(E_list(n), soe(n,i_t), P(n,i_t), dT);
                end
            end
        end
    end
end
soe_fin_real = soe_fin_real(:,1:2);
P_tot = sum(P);

%% evaluation of costs
% mobility 
E_mob = (E_list*ones(1,2)).*max(0,soe_fin_list - soe_fin_real);
costs(1) = sum(E_mob(:));
% non renewable energy
E_nowind = dT*max(0,P_tot'-P_w);
costs(2) = sum(E_nowind);
% losses
costs(3) = dT*sum(P_loss(:));

end

function [P_prev, u_prev] = warm_start(u,P,i_t,ind,u_init,P_init)
if (i_t>1)
    u_prev = u(i_t-1);
    P_prev = P(ind,i_t-1);
else
    u_prev = u_init;
    P_prev = P_init(ind);
end
end