function WindGenTotalLoadYTD2017 = import_prod_2017_1(workbookFile, sheetName, startRow, endRow)
%IMPORTFILE Import data from a spreadsheet
%  WINDGENTOTALLOADYTD2017 = IMPORTFILE(FILE) reads data from the first
%  worksheet in the Microsoft Excel spreadsheet file named FILE.
%  Returns the numeric data.
%
%  WINDGENTOTALLOADYTD2017 = IMPORTFILE(FILE, SHEET) reads from the
%  specified worksheet.
%
%  WINDGENTOTALLOADYTD2017 = IMPORTFILE(FILE, SHEET, STARTROW, ENDROW)
%  reads from the specified worksheet for the specified row interval(s).
%  Specify STARTROW and ENDROW as a pair of scalars or vectors of
%  matching size for dis-contiguous row intervals.
%
%  Example:
%  WindGenTotalLoadYTD2017 = importfile("/home/lglroman/Dropbox/SATIE projets recherche/pscc2022/codes/WindGenTotalLoadYTD_2017.xls", "January-June", 27, 52142);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 23-Jun-2021 14:02:17

%% Input handling

% If no sheet is specified, read first sheet
if nargin == 1 || isempty(sheetName)
    sheetName = 1;
end

% If row start and end points are not specified, define defaults
if nargin <= 3
    startRow = 27;
    endRow = 52142;
end

%% Setup the Import Options
opts = spreadsheetImportOptions("NumVariables", 1);

% Specify sheet and range
opts.Sheet = sheetName;
opts.DataRange = "C" + startRow(1) + ":C" + endRow(1);

% Specify column names and types
opts.VariableNames = "TOTALWINDGENERATIONINBPACONTROLAREAMWSCADA79687";
opts.VariableTypes = "double";

% Import the data
WindGenTotalLoadYTD2017 = readtable(workbookFile, opts, "UseExcel", false);

for idx = 2:length(startRow)
    opts.DataRange = "C" + startRow(idx) + ":C" + endRow(idx);
    tb = readtable(workbookFile, opts, "UseExcel", false);
    WindGenTotalLoadYTD2017 = [WindGenTotalLoadYTD2017; tb]; %#ok<AGROW>
end

%% Convert to output type
WindGenTotalLoadYTD2017 = table2array(WindGenTotalLoadYTD2017);
end