function Prob_xi = prob_xi_update(XI,P,u,AA,a,Prob_xi,variable)
% updates the matrix of transition probability of xi, based on historical
% records of xi and of a possible covariable

% inputs : 
%   XI: vector of the discretized value of the coordination
%             coefficient
%   P : historical data of charging power
%   u : historical data of dual variable
%   AA: vector of the discretized value of the covariable
%   a: historical data of covariable
%   Prob_xi: previous transition matrix
%   variable: string of the covariable to take into account

% outputs : 
%   Prob_xi: updated transition matrix

[N_xi,N_soe_fin,N_soe,N_t,~] = size(Prob_xi);

if strcmp(variable,'xi')
    prob_xi = zeros(length(XI));
    [N_days,N_ev,~] = size(P);
    P_serie = zeros(N_ev,N_days*N_t);
    for d = 1:N_days
        P_serie(:,(N_t*(d-1)+1:N_t*d)) = squeeze(P(d,:,:));
    end
    u_serie = reshape(u',[],1);
    xi_serie = P_serie - ones(N_ev,1)*u_serie';
    xi_serie = min(XI(end),max(xi_serie,XI(1)));
    
    d_xi = XI(2) - XI(1);
    for i_xi = 1:N_xi
        ind_xi = find((XI(i_xi)-d_xi/2<=xi_serie(:,1:end-1))&...
            (xi_serie(:,1:end-1)<XI(i_xi)+d_xi/2));
        ind_next = ind_xi + N_ev;
        for j = 1:N_xi
            ind2 = (XI(j)-d_xi/2<=xi_serie(ind_next))&...
                (xi_serie(ind_next)<XI(j)+d_xi/2);
            prob_xi(i_xi,j) = sum(ind2);
        end
        if sum(prob_xi(i_xi,:))==0
            prob_xi(i_xi,floor(length(XI)/2)+1) = 1;
        else
            prob_xi(i_xi,:) = ...
                prob_xi(i_xi,:)/sum(prob_xi(i_xi,:));
        end
    end
    Prob_xi = zeros(size(Prob_xi));
    for i_soe_fin = 1:N_soe_fin
        for i_soe = 1:N_soe
            for i_t = 1:N_t
                Prob_xi(:,i_soe_fin,i_soe,i_t,:) = prob_xi;
            end
        end
    end
else
    prob_xi = zeros(length(XI),length(AA),length(XI));
    [N_days,N_ev,~] = size(P);
    P_serie = zeros(N_ev,N_days*N_t);
    a_serie = zeros(N_ev,N_days*N_t);
    for d = 1:N_days
        P_serie(:,(N_t*(d-1)+1:N_t*d)) = squeeze(P(d,:,:));
        a_serie(:,(N_t*(d-1)+1:N_t*d)) = squeeze(a(d,:,:));
    end
    u_serie = reshape(u',[],1);
    xi_serie = P_serie - ones(N_ev,1)*u_serie';
    xi_serie = min(XI(end),max(xi_serie,XI(1)));
    
    d_xi = XI(2) - XI(1);
    d_a = abs(AA(2)-AA(1));
    for i_xi = 1:N_xi
        ind_xi = (XI(i_xi)-d_xi/2<=xi_serie(:,1:end-1))&...
            (xi_serie(:,1:end-1)<XI(i_xi)+d_xi/2);
        for i_a = 1:length(AA)
            ind_a = (AA(i_a)-d_a/2<=a_serie(:,1:end-1))&...
                (a_serie(:,1:end-1)<AA(i_a)+d_a/2);
            ind = find(ind_xi.*ind_a);
            ind_next = ind + N_ev;
            for j = 1:N_xi
                ind2 = (XI(j)-d_xi/2<=xi_serie(ind_next))&...
                    (xi_serie(ind_next)<XI(j)+d_xi/2);
                prob_xi(i_xi,i_a,j) = sum(ind2);
            end
            if sum(prob_xi(i_xi,i_a,:))==0
                prob_xi(i_xi,i_a,floor(length(XI)/2)+1) = 1;
            else
                prob_xi(i_xi,i_a,:) = ...
                    prob_xi(i_xi,i_a,:)/sum(prob_xi(i_xi,i_a,:));
            end
        end
    end
    
    switch variable
        case 'xi'
            Prob_xi = zeros(size(Prob_xi));
            for i_soe_fin = 1:N_soe_in
                for i_soe = 1:N_soe
                    for i_t = 1:N_t
                        Prob_xi(:,i_soe_fin,i_soe,i_t,:) = prob_xi;
                    end
                end
            end
        case 'soe_fin'
            Prob_xi = zeros(size(Prob_xi));
            for i_soe = 1:N_soe
                for i_t = 1:N_t
                    Prob_xi(:,:,i_soe,i_t,:) = prob_xi;
                end
            end
        case 'soe'
            Prob_xi = zeros(size(Prob_xi));
            for i_soe_fin = 1:N_soe_fin
                for i_t = 1:N_t
                    Prob_xi(:,i_soe_fin,:,i_t,:) = prob_xi;
                end
            end
        case 'T_charg'
            Prob_xi = zeros(size(Prob_xi));
            for i_soe_fin = 1:N_soe_fin
                for i_soe = 1:N_soe
                    Prob_xi(:,i_soe_fin,i_soe,:,:) = prob_xi;
                end
            end
    end
end
end