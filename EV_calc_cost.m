function tot_cost = EV_calc_cost(P, soe, SOE, E, xi, prob_xi, DeltaT, J_future)
% intermediate function to compute the expected total cost = 
% instantaneous + future_cost*(its probability).
% inputs : P : exchanged power
%          soe : current state of energy
%          SOE : set of possible states of energy
%          E : vehicle battery capacity
%          xi : value of the coordination coefficient
%          prob_xi : vector of the probability of the future value of
%                   xi
%          DeltaT : time step in hours
%          J_future : cost of the future state (2D matrix)
% output : total_cost scalar value

rho = 1e-6;

%% dynamic of the EV battery
[soe_next, P_loss, P_ev] = EV_dynamic(E, soe, P, DeltaT);

%% instantaneous cost
C_loss = DeltaT*P_loss;
C_coord = rho/2*(P_ev - xi)^2;
inst_cost = C_loss + C_coord;

%% future cost
future_cost = Interp_Cost(SOE,J_future,soe_next);
future_cost = future_cost*prob_xi;

%% total cost
tot_cost = inst_cost + future_cost;

end