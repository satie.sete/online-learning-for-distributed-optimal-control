function [P,u] = sharing(dT, P_w, EE, SOE_fin, SOE, XI, TT, U_law, EV_strat,...
        E, soe_fin, soe, t, P_prev, u_prev)
% performs the ADMM resolution of the fleet sharing problem for a single
% time step
% inputs : 
%   dT : time step
%   P_w : current wind power
%   EE : vector of the discretized value of the battery capacity
%   SOE_fin : vector of the discretized value of desired final state of energy
%   SOE : vector of the discretized value of states of energy
%   XI : vector of the discretized value of the coordination coefficient
%   TT : vector of the discretized value of time vector
%   U_law : matrix of the optimal policy
%   EV_strat : strategy to be used by each vehicle
%   E : list of capacity of each vehicle
%   soe_fin : list of desired soe of each vehicle
%   soe : list of current soe of each vehicle
%   t : list of remaining charging time of each vehicle
%   P_prev : previous charging power (warm start)
%   u_prev : previous dual variable (warm start)
% outputs : 
%   P : charging power of each vehicle
%   u : dual variable

rho = 1e-6;
n_ev = length(E);
% stopping criteria tolerance
rel_tol = 0.01;
abs_tol = 1;
max_iter = 5000;
% results preallocation
P = zeros(max_iter,n_ev);
P_mean = zeros(max_iter,1);
Q = zeros(max_iter,1);
u = zeros(max_iter,1);
% initialisation
P(1,:) = P_prev;
P_mean(1) = mean(P(1,:));
u(1) = u_prev;
for k = 1:max_iter
    % P update
    P(k+1,:) = P_update(n_ev,P(k,:),P_mean(k),Q(k),u(k),...
        EE,SOE_fin,SOE,XI,TT,U_law,EV_strat,E,soe_fin,soe,t);
    P_mean(k+1) = mean(P(k+1,:));
    % Q update
    Q(k+1) = Q_update(u(k),P_mean(k+1),P_w,n_ev,dT,rho);
    % dual variable update
    u(k+1) = u(k) + P_mean(k+1) - Q(k+1);
    % stopping criterion
    tol = abs_tol+max(abs(P(k+1,:)))*rel_tol;
    if (norm(P(k+1,:)-P(k,:))<tol)...
            &&(norm(P_mean(k+1)-Q(k+1))<tol)
        break
    end
end
if k==max_iter
    str = strcat('reached max iter, dual residue=',...
        num2str(norm(P_mean(k+1)-Q(k+1))));
    disp(str)
end
P = P(k+1,:);
u = u(k+1);
end

function P = P_update(n_ev,P,P_mean,Q,u,EE,SOE_fin,SOE,XI,TT,U_law,EV_strat,...
    E,soe_fin,soe,t)
    for n = 1:n_ev
        xi = P(n) - P_mean + Q - u;
        P(n) = Interp_Strategy(EE, SOE_fin, SOE, XI, TT, ...
            U_law, EV_strat(n),...
            E(n), soe_fin(n), soe(n), xi, t(n));
    end
end

function Q = Q_update(u,P_mean,P_w,n_ev,dT,rho_ev)
% minimisation by a Nelder Mead simplex

% simplex parameters default values
alpha = 1;
gamma = 2;
rho = -0.5;
sigma = 0.5;
rel_tol = 1e-5;
abs_tol = 1e-2;
% function to be minimized
obj_fun = @(Q)(dT*max(n_ev*Q-P_w,0) + n_ev*rho_ev/2*(Q - u - P_mean)^2);
x_max =  100e3;
x_min = -100e3;
X = [x_min x_max];
F = zeros(size(X));
n=1;
cond = 1;
while cond
    X0=X;
    % EVALUATION
    for i = 1:length(X)
        F(i) = obj_fun(X(i));
    end
    % ORDERING
    [F,ind] = sort(F);
    X = X(ind);
    % CENTROID
    % as it is 1D, centroid is the first point
    x0 = X(1);
    % REFLECTION
    xr = x0 + alpha*(x0-X(2));
    xr = max(min(xr,x_max),x_min);
    if obj_fun(xr) < F(1)
        % EXPANSION
        xe = x0 + gamma*(x0-X(2));
        xe = max(min(xe,x_max),x_min);
        if obj_fun(xe)<obj_fun(xr)
            X = [X(1); xe];
        else
            X = [X(1); xr];
        end
    else
        % CONTRACTION
        xc = x0 + rho*(x0-X(2));
        xc = max(min(xc,x_max),x_min);
        if obj_fun(xc)<F(2)
            X = [X(1) ; xc];
        else
            % REDUCTION
            X(2) = X(1) + sigma*(X(2)-X(1));
        end
    end
    n=n+1;
    tol = abs_tol + min(abs(X))*rel_tol;
    cond = (norm(X-X0)>tol)&(n<100);
end
Q = X(1);
end