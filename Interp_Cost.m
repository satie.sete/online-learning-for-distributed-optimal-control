function future_cost = Interp_Cost(SOE,V,soe_next)
% specifically designed interpolation function for the future cost

% input : SOE : set of possible states of energy
%         V : cost of the future state (2D matrix)
%         soe_next : value of the future state of energy, 
%                    value where future_cost is interpolated
% output : future_cost : 1D vector of the future cost for the soe_next value

Delta_SOE = SOE(2)-SOE(1);
i_soe_next = find(SOE>=soe_next,1);
if i_soe_next<=1
    future_cost = V(1,:) + ...
        (soe_next - SOE(1))/Delta_SOE*(V(2,:)-V(1,:));
else
    future_cost = V(i_soe_next-1,:)+ ...
        (soe_next-SOE(i_soe_next-1))/Delta_SOE*...
        (V(i_soe_next,:)-V(i_soe_next-1,:));
end
end