% gathers parameters for the online learning iterative update

% How many vehicles to update at a time
online_prms.share = 5;
online_prms.N_ev = N_ev/online_prms.share; 

% How often to update
online_prms.N_days = 1;

% Do vehicle share information ? 
online_prms.share_info = 1; 
% 0 if a vehicle update is based on its own records
% 1 if a vehicle update is based on records from all the fleet

% How to decide which vehicles to update ?
online_prms.ev_choice = 1;
% 0 if vehicles are randomly selected for strategy updates
% 1 if vehicles are regularly selected for strategy updates

% specify the order of update if online_prms.ev_choice==1
tmp = ones(online_prms.N_ev,1)*(1:floor(N_ev/online_prms.N_ev)+1);
tmp = reshape(tmp,[],1);
online_prms.ev_choice_order = tmp(1:N_ev);

% current number of strategy updates
online_prms.k = 1;
