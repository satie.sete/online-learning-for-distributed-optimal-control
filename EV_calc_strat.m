function U_law = EV_calc_strat(EE,SOE_fin,SOE,TT,XI,Prob_xi)
% runs the optimal strategy computation by stochastic dynamic programming

%% Optimisation call
% tic;
[U_law,~] = EV_StoDynProg(EE,SOE_fin,SOE,XI,TT,Prob_xi);
% toc

end