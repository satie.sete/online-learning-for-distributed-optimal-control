function [P_opt,min_cost,ret] = EV_DP_Minimisation(E, soe, SOE, ...
    xi, prob_xi, DeltaT, J_future)

% compute the optimal charging power and the associated optimal cost for a
% given state vector configuration
% inputs : E : vehicle battery capacity
%          soe : current state of energy
%          SOE : discretized set of state of energy values
%          xi : current coordination coefficient
%          prob_xi : vector of the probability of the future value of
%                   xi
%          DeltaT : time step in hours
%          J_future : cost of the future state (2D matrix)
% output : P_opt : optimal power
%          min_cost : minimal cost associated to the optimal power
%          ret : =1 if everything ok
%                =-1 if NLsimplex not converged or local optimum

% power boundaries
[P_min,P_max] = EV_constraint_P(soe,E,DeltaT);
% Nelder Mead Simplex with multiple initialization
[P_opt1,min_cost1,ret1] = NMsimplex([P_min;P_max], P_min, P_max, ...
    soe, SOE, E, xi, prob_xi, DeltaT, J_future);
[P_opt2,min_cost2,ret2] = NMsimplex([(P_min+P_max)/2;P_max], P_min, P_max, ...
    soe, SOE, E, xi, prob_xi, DeltaT, J_future);
[P_opt3,min_cost3,ret3] = NMsimplex([P_min;(P_min+P_max)/2],  P_min, P_max, ...
    soe, SOE, E, xi, prob_xi, DeltaT, J_future);
% convergence tests
J_future = [min_cost1 ; min_cost2; min_cost3];
P = [P_opt1;P_opt2;P_opt3];
ret = [ret1;ret2;ret3];
[min_cost,ind_tmp] = min(J_future);
P_opt = P(ind_tmp);
ret = ret(ind_tmp);
end