function [Prob_xi,EV_strat] = prob_xi_online(XI,P,u,AA,a,Prob_xi,...
    EV_strat,online_prms,variable)
% updates the matrix of transition probability of xi, based on historical
% records of xi and of a possible covariable

% inputs : 
%   XI: vector of the discretized value of the coordination
%             coefficient
%   P : historical data of charging power
%   u: historical data of dual variable
%   AA: vector of the discretized value of the covariable
%   a: historical data of covariable
%   Prob_xi: previous transition matrix
%   EV_strat: strategy to be used by each vehicle
%   online_prms: online learning parameters 
%   variable: string of the covariable to take into account
% outputs
%   Prob_xi: updated transition matrix
%   EV_strat : updated list of strategy to be used by each vehicle

[~,N_ev,~] = size(P);
% how to decide which vehicles to update
if online_prms.ev_choice==0
    % randomly
    ind = randi(N_ev,online_prms.N_ev,1);
    EV_strat(ind) = online_prms.k;
else
    % regularly, as indicated in ev_choice_order
    EV_strat(online_prms.ev_choice_order==online_prms.k) = online_prms.k;
end
% is information shared ?
if online_prms.share_info==1
    % within the entire fleet
    Prob_xi = prob_xi_update(XI,P,u,AA,a,Prob_xi,variable);
else
    % only within a subgroup
    Prob_xi = prob_xi_update(XI,...
        P(:,online_prms.ev_choice_order==online_prms.k,:), ...
        u, ...
        AA,a(:,online_prms.ev_choice_order==online_prms.k,:),...
        Prob_xi,variable);
end
end