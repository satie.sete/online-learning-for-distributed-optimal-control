function [soe_next, P_loss, P_ev] = EV_dynamic(E, soe, P, DeltaT)
  % computing the next state of energy of the battery. 
  % inputs
  %   E : battery capacity
  %   soe : initial state of energy
  %   P : exchanged desired power
  %   dT : time step (in hours)
  % output
  %   soe_next : state of energy at the end of the time step
  %   P_loss : power of the losses
  %   P_ev : actual exchanged power
  
  alpha = 0.1/150e3;
  P_loss = alpha*P.^2;
  if P>0 % charging case
      P_ev = P - P_loss;
      soe_next = soe + DeltaT.*P_ev./E;
  else
      P_ev = P + P_loss;
      soe_next = soe + DeltaT.*P./E;
  end
  % saturations
  if soe_next>1
      P_loss = P_loss + E*(soe_next-1);
      soe_next = 1;
  elseif soe_next<0
      P_ev = P_ev + E*soe_next;
      soe_next = 0;
  end
end