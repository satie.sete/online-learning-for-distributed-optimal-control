function U = Interp_Strategy(EE, SOE_fin, SOE, XI, TT, U_law, EV_strat,...
        E, soe_fin, soe, xi, t)
% specifically designed interpolation function for the optimal strategy

% input : EE : set of possible battery capacity
%         SOE_fin : set of possible desired final state of energy
%         SOE : set of possible states of energy
%         XI : vector of the discretized value of the coordination
%              coefficient
%         TT : discretized time vector
%         U_law : matrix of the optimal policy
%         E : vehicle battery capacity
%         soe_fin : vehicle desired final state of energy
%         soe : vehicule current state of energy
%         xi : current value of the coordination coefficient
%         t : current remaining charging time
% output : U : optimal charging power according to the charging strategy

%% index search
tol = 1e-12;
% i_EE = find(EE-tol>=E,1);
i_EE = find(EE>=E-tol,1);
% i_soe_fin = find(SOE_fin-tol>=soe_fin,1);
i_soe_fin = find(SOE_fin>=soe_fin-tol,1);
% i_soe = find(SOE-tol>=soe,1);
i_soe = find(SOE>=soe-tol,1);
% i_xi = find(XI-tol>=xi,1);
i_xi = find(XI>=xi-tol,1);
% i_t = find(TT-tol<=t,1);
i_t = find(TT<=t+tol,1);

%% particular cases and warnings
if i_EE == 1 % only occurs if EE(1)==E
    i_EE = 2; % will not affect the interpolated value
end
if i_soe_fin == 1 % only occurs if SOE_fin(1)==soe_fin
    i_soe_fin = 2; % will not affect the interpolated value
end
if i_soe == 1 % only occurs if SOE(1)==soe
    i_soe = 2; % will not affect the interpolated value
end
if i_xi == 1 % coordination coefficient smaller than expected
    % warning('reached minimum value of xi')
    % the extreme values of xi included in the strategy are sufficient to 
    % force a discharge at maximum power. The current value can therefore 
    % be clipped
    i_xi = 2;
elseif i_xi==length(XI)
    % warning('reached maximum value of xi')
end
if i_t == 1 % charging time longer than expected
    % warning('reached maximum value of charging time')
    t = TT(1); % charging time limited to maximum handled value
    i_t = 2;
end

%% recursive interpolation
% interpolation along battery capaciy E
dEE = EE(i_EE) - EE(i_EE-1);
U = U_law(EV_strat,i_EE-1,i_soe_fin-1:i_soe_fin,i_soe-1:i_soe,i_xi-1:i_xi,i_t-1:i_t) + ...
    (E-EE(i_EE-1))/dEE*...
    (U_law(EV_strat,i_EE ,i_soe_fin-1:i_soe_fin,i_soe-1:i_soe,i_xi-1:i_xi,i_t-1:i_t)-...
    U_law(EV_strat,i_EE-1,i_soe_fin-1:i_soe_fin,i_soe-1:i_soe,i_xi-1:i_xi,i_t-1:i_t));
% interpolation along final state of energy soe_fin
dSOE_fin = SOE_fin(2)-SOE_fin(1);
U = U(1,1,1,:,:,:) + ...
    (soe_fin - SOE_fin(i_soe_fin-1))/dSOE_fin*...
    (U(1,1,2,:,:,:)-U(1,1,1,:,:,:));
% interpolation along current state of energy soe
dSOE = SOE(2)-SOE(1);
U = U(1,1,1,1,:,:) + ...
    (soe - SOE(i_soe-1))/dSOE*...
    (U(1,1,1,2,:,:)-U(1,1,1,1,:,:));
% interpolation along coordination coefficient xi
dXI = XI(2)-XI(1);
U = U(1,1,1,1,1,:) + ...
    (xi - XI(i_xi-1))/dXI*...
    (U(1,1,1,1,2,:)-U(1,1,1,1,1,:));
% interpolation along remaining charging time t
dT = TT(2)-TT(1);
U = U(1,1,1,1,1,1) + ...
    (t - TT(i_t-1))/dT*...
    (U(1,1,1,1,1,2)-U(1,1,1,1,1,1));
U = squeeze(U);

end