function final_cost = EV_final_cost(SOE_fin,SOE)
% vehicule final cost function.
% The satisfaction of the user is set by the desired final state of energy
% inputs : SOE_fin : vector of desired final state of energy
%          SOE : vector of possible state of energy          
% output : final_cost : vector of satisfaction for all value of SOE

final_cost = SOE_fin*ones(1,length(SOE)) - ones(length(SOE_fin),1)*SOE';
final_cost = max(0,final_cost);
end