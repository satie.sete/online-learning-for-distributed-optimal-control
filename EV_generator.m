function [H_list, E_list, soe_0_list, soe_fin_list, fleet_prms] = ...
    EV_generator(N_ev,N_days)
% generator of vehicles behavior based on
%   - the report "les vehicules electriques en perspective",
%       Commissariat du developpement durable p44
%   - most sold electric cars in France 2014, "Tableau de bord automobile",
%       Comite des constructeurs francais d'automobiles
% Each vehicle is described by a battery capacity E (Wh) and a daily 
% commute of two trips a day. H describes the morning leaving time, morning
% arrival time, evening leaving time and leaving arrival time of each
% vehicule for each day. SOE_0 is the initial state of energy of each
% vehicucle for each day and each trip. SOE_fin is the desired final state
% of energy of each vehicucle for each day and each trip. 
% inputs:
%   N_ev : number of vehicles
%   N_days : length of the scenario in days
% outputs :
%   H : matrix (N_days,N_ev,4) of the arriving and leaving hours
%   E : matrix (N_days,N_ev) of the battery capacities (kWh)
%   SOE_0 : matrix (N_days,N_ev,2) of the initial states of energy
%   SOE_fin : matrix (N_days,N_ev,2) of the desired final states of energy
%   fleet_prms : structure with required statistical informations for the
%     management.

%%%%%%%%%%%%%%%%%%%%
% SHAPPING OF DATA %
%%%%%%%%%%%%%%%%%%%%

% time step (fixed by the available data set)
dT = 0.5;
T = dT:dT:24;
fleet_prms.T = T;
% departure time in week day morning p44
P_am1 = [0;0;0;0;0;0.2;0.2;0.4;1;1.5;1.5;2.1;2.5;5.8;10.5;15;12.2;9;3.8;1.8;1;
    0.9;1;0.9;1.3;3.2;7.2;7.2;2.1;1;0.9;0.6;0.8;0.6;0.8;0.6;0.5;0.2;0.2;
    0.4;0.5;0.4;0.1;0.1;0;0;0;0]./100;
P_am1 = P_am1./sum(P_am1);
P_am1 = cumsum(P_am1);
% arriving time in week day morning p44
P_am2 = [0;0;0;0;0;0;0.1;0.2;0.4;1.8;2.1;2.1;1.8;4;6.5;13.5;14;11.9;7;2.1;
    1.7;1;1;1.2;1.2;2.2;5.5;8;3.8;1.2;1;0.6;0.8;0.6;0.8;0.6;0.4;0.3;0.2;
    0.3;0.3;0.4;0.1;0.2;0;0;0;0]./100;
P_am2 = P_am2./sum(P_am2);
fleet_prms.P_in = P_am2;
P_am2 = min(P_am1,cumsum(P_am2));
% departure time in week day evening p45
P_pm1 = [0.2;0.2;0.1;0.1;0.1;0;0;0;0.1;0;0.2;0.1;0.2;0.3;0.5;0.5;0.9;1.5;1.5;
    1.5;2.5;2.8;3.5;4.8;7.5;2.9;2.1;1.5;1.5;1.5;2.5;2.6;4.6;5.7;7.6;6.7;7.5;
    5.5;5.2;3.2;2.8;1.5;1.6;0.9;1.1;1;0.9;0.5]./100;
P_pm1 = P_pm1./sum(P_pm1);
fleet_prms.P_out = P_pm1;
P_pm1 = min(P_am2,cumsum(P_pm1));
% arriving time in week day evening p45
P_pm2 = [0.3;0.2;0.1;0.1;0.1;0.1;0.1;0;0.1;0.1;0.2;0.2;0.2;0.3;0.4;0.5;0.7;
    1.1;1.5;1.4;2;2.5;3.1;4.1;7.8;3.9;2.1;1.8;1.5;1.5;2;2.5;3.5;5;6.4;6.5;
    7.8;6.9;5.8;4.5;3.5;2.1;1.5;1;1;1;0.9;0.7]./100;
P_pm2 = P_pm2./sum(P_pm2);
P_pm2 = min(P_pm1,cumsum(P_pm2));
% daily commuting distance p37
D = [5:5:100, 105];
P_d = [1;2;4;5;5;5;5;5;5;4;4;4;3;3;2;2;2;1.5;1.5;1.5;36]./100;
P_d = P_d./sum(P_d);
% the last number represents all trips further than 100km
eta = 420/85000; % km/kWh
% efficiency of Tesla S

% probabilities of battery capacities
% market share of EV
P_sell = [56.5;15.2;11.1;4.8;3.1;2.5;1.8;1.5;1.5;0.8;0.6;0.1]./100;
P_sell = P_sell./sum(P_sell);
% capacities of most sold EV (Wh)
E_sell = [22;24;30;17.6;85;18.7;22;15;16;24.2;27;28]*1e3;
% formating of capacity data
dE = 1e3;
[E_sell_max,~] = max(E_sell);
EE = dE:dE:E_sell_max;
P_E = zeros(length(EE)-1,1);
for w=1:length(P_E)
    ind = (E_sell<=EE(w+1))&(E_sell>EE(w));
    P_E(w) = sum(P_sell(ind));
end
P_E = P_E./sum(P_E);
fleet_prms.P_E = P_E;

%%%%%%%%%%%%%%%
% RANDOM DRAW %
%%%%%%%%%%%%%%%
% vehicles are supposed to do a daily commute : 2 trips per day. 

H_list = zeros(N_days,N_ev,4);
soe_0_list = zeros(N_days,N_ev,2);
soe_fin_list = zeros(N_days,N_ev,2);
% vehicles capacities do not change between one day and the following.
E_list = zeros(N_ev,1);

for n=1:N_ev
    % battery capacity
    a =rand(1);
    a_ind = find((cumsum(P_E)>a),1,'first');
    a_ind = max(1,min(a_ind,length(P_E)));
    E_list(n) = EE(a_ind);
end
for d=1:N_days
    for n = 1:N_ev
        % morning departure time
        a = rand(1);
        a_ind = find(P_am1>a,1,'first');
        a_ind = max(1,min(a_ind,length(T)));
        H_list(d,n,1) = T(a_ind);
        % morning arrival time
        a = a+(1-a).*rand(1);
        a_ind = find(P_am2>a,1,'first');
        a_ind = max(1,min(a_ind,length(T)));
        H_list(d,n,2) = T(a_ind);
        % evening departure time
        a = a+(1-a).*rand(1);
        a_ind = find(P_pm1>a,1,'first');
        a_ind = max(1,min(a_ind,length(T)));
        H_list(d,n,3) = T(a_ind);
        % evening departure time
        a = a+(1-a).*rand(1);
        a_ind = find(P_pm2>a,1,'first');
        a_ind = max(1,min(a_ind,length(T)));
        H_list(d,n,4) = T(a_ind);
        % initial state of energy for trip 1
        a = rand(1);
        a_ind = find(cumsum(P_d)>a,1,'first');
        a_ind = max(1,min(a_ind,length(P_d)));
        dist = D(a_ind);
        if dist==105
            % if the trip is longer than 100km, there is no statistics
            % available to describe the trip length. A random distance
            % between 100km and 200km is drawn.
            dist = 100+100*rand();
        end
        cons = dist/eta;
        soe_0_list(d,n,1) = max(1-cons/E_list(n),0);
        % desired final state of energy
        soe_fin_list(d,n,1) = soe_0_list(d,n) + (1-soe_0_list(d,n))*rand();
        % initial state of energy for trip 2
        a = rand(1);
        a_ind = find(cumsum(P_d)>a,1,'first');
        a_ind = max(1,min(a_ind,length(P_d)));
        dist = D(a_ind);
        if dist==105
            % if the trip is longer than 100km, there is no statistics
            % available to describe the trip length. A random distance
            % between 100km and 200km is drawn.
            dist = 100+100*rand();
        end
        cons = dist/eta;
        soe_0_list(d,n,2) = max(1-cons/E_list(n),0);
        % desired final state of energy
        soe_fin_list(d,n,2) = soe_0_list(d,n) + (1-soe_0_list(d,n))*rand();
    end
end
end